..    Пролетарии всех стран, соединяйтесь!
      Proletoj el ĉiuj landoj, unuiĝu!
.. vim: syntax=rst
.. _contrib:
.. highlight:: bash

Руководство для начинающего контрибьютора
=========================================

Покажу пример создания форка на примере нового фронта Сириусо. Кто не
знает это разрабатываемый нами движок нашей Умной социальной сети
Технокома. С другими проектами / репозиториями всё аналогично.

Добавление в избранное
----------------------

В данном случае я иду в этот репозиторий нового фронта Сириусо
https://gitlab.com/tehnokom/siriuso-fronto

В верхнем правом углу есть кнопки добавления в избранное (типа лайк) и
как раз Fork. Сразу всем напомню, что когда вы работаете с каким-то
репозиторием / проектом, то добавляйте его в избранное, во-первых: его
потом будет легче находить через Избранное, во-вторых: это повышает его
статус на ГитЛабе, показывает другим людям что над этим проектом
работают и он кому-то интересен. Поэтому мой первый скриншот, о том что
нужно вот сюда тыкнуть на Избранное (лайкнуть)

|image0|

Повторюсь, не только в этом репозитории на который я выше дал ссылку, а
во всех с которыми вы работаете.

Создание форка
--------------

Дальше переходим к созданию форка. Жмём там рядом кнопку Fork.

|image1|

На следующем экране у вас спросит куда вы хотите сделать этот форк (эту
копию, это ответвление). У многих будет только 1 вариант, их
пользовательские репозитории, потому что у многих не будет админских
прав на какую-то группу в ГитЛабе, в общем делаем в личных репозиториях.

|image2|

После этого сразу создаётся ответвление и вас перекидывает в такой же
репозиторий, ну уже в вашей личной группе репозиториев. Ну то есть может
показаться что ничего не произошло, но на самом деле уже всё произошло,
и вы теперь находитесь не в репозиториях Технокома, а там в начале пути
сверху ваше имя написано и ниже табличка что всё случилось.

|image3|

Чтобы понять какие есть преимущества у использования форков, рекомендую
прочитать, например, вот эту статью, где кроме технической информации,
автор "по ролям" приводит жизненные ситуации, чтобы было понятно зачем
нужны форки и чем они удобны https://webdevkin.ru/posts/raznoe/git-fork

Выполнение мерджреквеста
------------------------

Товарищи, а теперь давайте посмотрим, как делать мердж реквесты при
работе с форком. Ну то есть то что вы делали локально, вам как всегда
нужно выгрузить в ГитЛаб, ну только теперь в репозиторий своего форка. А
дальше чтобы влить вашу работу в ветку develop в основном проекте, вам
нужно будет пойти в ГитЛабе, в раздел мердж реквестов именно своего
форка.

|image4|

Там как и раньше делали в главном проекте нажать на "зелёную кнопку"

|image5|

И дальше в качестве источника выбрать ваш форк и ветку в вашем форке, а
в качестве результирующего выбрать главный проект и нужную там ветку, в
большинстве случаев это будет, как вы помните ветка develop.

|image6|

Ну а дальше всё тоже самое заполнили все поля, ну то есть обычно просто
написали понятный заголовок и описание и нажали снова на "зелёную
кнопку"

|image7|

И после этого, мердж реквест появится в списке мерджреквестов в основном
проекте, ну то есть не ищите его на странице вашего форка, вы же
оформили его в основной проект, вот там он и появится.

Обновление форка
----------------

Теперь пару слов о том, как обновить свой форк из главного репозитория.

1. Вообще, конечно, можно свой старый форк полностью удалить (или
   отвязать в отдельный проект) и сделать новый форк, но такое делается
   когда в основном проекте прям в разных ветках дофига изменений, а в
   вашем форке ничего нового нет. А если есть то, конечно можно вынести
   в отдельный проект.

2. Но можно просто одну ветку обновить. Это можно сделать через мердж
   реквест тоже, только в обратном порядке Ну то есть идёте в главный
   проект, там нажимаете сделать мердж реквест из главного проекта,
   обычно из ветки develop, в свой форк в ветку develop. После этого уже
   в ваших твоих личных мердж реквестах он появится, там нужно принять
   мерджреквест, тыкайте в нём зелёную кнопку и всё обновится. Ну это
   если нет слишком сильного конфликта кода. Если есть, то тогда проще
   может быть удалить старый форк и сделать новый.

Вот про обновление ветки на примере проекта Универсо. Если находитесь в
ГитЛабе в своём форке, то жмёте по ссылке и переходите в главный проект.

|image8|

В главном проекте, заходите в мерджреквесты, в русском переводе "Запросы
на слияние"

|image9|

Там традиционно тыкаете "большую зелёную кнопку" и создаёте новый
мерджреквест

|image10|

Выбираете в источнике основной проект и ветку, напомню, обычно это ветка
develop и в результирующем проекте выбираете свой форк и там ветку,
соответственно это обычно будет тоже develop. И жмёте очередную "большую
зелёную кнопку".

|image11|

Поскольку это в целом техническое обновление, то дальше в карточке можно
особо не заполнять заголовок и описание, тыкаете внизу "большую зелёную
кнопку".

|image12|

Вас после этого перекидывает в мерджреквесты в вашем форке и там жмите
"большую зелёную кнопку". Если её нет, значит проблема сложнее, значит
тут или разруливать или действительно удалять форк и делать новый.

|image13|

Удаление форка
--------------

Теперь о том как удалить или отвязать форк.

Сначала проверяете что это точно форк, смотрите что это в вашем
репозитории и что написано откуда это ответвлено. А то у некоторых могут
быть права на основной репозиторий Потом идёте в Настройки - Основные

|image14|

Там проматываете вниз и в разделе Расширенные, нажимаете Развернуть.

|image15|

Там будет 2 варианта:

1. Самый нижний это удалить проект. Этот вариант используется если в
   вашем старом форке нет ничего важного. Например, вы уже из него в
   основной проект отправили мерджи или то что у вас было устарело.

2. А чуть выше есть вариант отвязать форк от основного проекта. То есть
   он перестанет считаться форком, а станет независимым проектом. После
   этого вы сможете сделать новый форк. Такой вариант используется если
   у вас в старом форке осталось что-то важное, но его прям долго
   обновлять и вам не хочется с этим возиться.

|image16|

При удалении надо всё равно отвязывать и переименовывать, потому что он
на неделю замораживает старый форк. Только потом удаляет

|image17|

Вот тут надо путь поменять предварительно перед удалением. то есть
отвязать, переименовать, поменять путь и после только удалять.

|image18|

|image19|

Краткий порядок действий
------------------------

1. Делаем свои форки

2. Удаляем из главного репозитория свои ветки, чтоб не разводить
   зоопарк.

3. В своём форке создаём ветку от develop и в ней работаем *(не работаем
   в develop!)*

4. Когда есть что смерджить, то

   1. **Обновляем свой форковый develop** из основного

   2. Мерджим свою форковую ветку со своим обновлённым develop

5. Отправляем мерджреквест своего develop в основной репозиторий

6. Или обновляем после выполненного мерджа свой develop, или пересоздаём
   форк (как описано выше). Удаляем свою рабочую ветку (можно еще ранее,
   в процессе мерджа)

7. Далее с пункта 3.

*PS: Желательно это всё делать почаще. Чтобы потом при мердже не вылезало
много конфликтов кода.*

Памятка по командам git
-----------------------

Клонируем проект из репозитория к себе на компьютер (под псевдонимом **origin**)

.. code-block:: bash

        git clone git@gitlab.com:{ваше имя пользователя gitlab}/siriuso-fronto.git

        или

        git clone https://gitlab.com/{ваше имя пользователя gitlab}/siriuso-fronto.git

Переходим в директорию с проектом

.. code-block:: bash

        cd siriuso-fronto

Переключаемся на ветку **develop**

.. code-block:: bash

        git checkout develop

**ВАЖНО!!!** Ветвление создается из того места где вы находитесь.
Соответственно в нашем случае надо **СНАЧАЛА** перейти в **develop**, а уже
потом создавать новую ветку.

По умолчанию после клонирования вы находитесь в **master**.

Переключаемся на рабочую ветку ( если её нет, то создастся
автоматически).

.. code-block:: bash

        git checkout -b perov

Или сначала создаём ветку

.. code-block:: bash

        git branch perov

А потом на неё переключаемся

Далее работаем, работаем, работаем.

Делаем коммит с описанием

.. code-block:: bash

        git commit -m 'описание изменений'

Если что забыли внести в коммит, чтоб не делать новый можно добавить в
предыдущий

.. code-block:: bash

        git commit --amend

Извлечь данные о всех изменениях в репозитории

.. code-block:: bash

        git fetch

Извлекает (**fetch**) данные с сервера, с которого вы изначально
склонировали, и автоматически пытается слить (**merge**) их с кодом, над
которым вы в данный момент работаете.

.. code-block:: bash

        git pull

Важно понимать что **git** **pull** это в большинстве случаев **git** **fetch** и **git**
**merge** выполненные подряд. Учебник по git рекомендует использовать именно
эту связку вместо **git** **pull**, "поскольку магия **git** **pull** может часто
сбивать с толку"

Загрузить изменения на сервер. Если в этот репозиторий кто-то другой уже
что то добавил, то будет отказано. Надо сначала извлечь новые данные
(**git** **pull**)

.. code-block:: bash

        git push origin develop

где **origin** (по умолчанию) название репозитория которое можно посмотреть
в **"git** **remote** **-v"**. Если **origin**, то можно не указывать.

**develop** название ветки в которую загружаются изменения. Если текущая, то
можно не указывать.

Объединение веток:

Сначала переключаемся на ветку **В** которую соединяем

.. code-block:: bash

        git checkout develop

Потом объединяем с той **ОТКУДА** вносим изменения

.. code-block:: bash

        git merge perov

Тут стоит сказать, что решать конфликты слияния в консоли не сильно
удобно без опыта. При конфликтах происходит внесение в результирующий
файл всех конфликтующих вариантов, и потом надо либо вручную их
поправить после чего для каждого такого файла выполнить **git** **add**, либо
воспользоваться **git** **mergetool** который проведёт по конфликтам помогая их
решить. После всех исправлений выполнить **git** **commit**.

Удаление ненужной ветки. Перед этим надо сначала переключиться на другую
(например **develop**). Если в этой ветке есть несмердженные изменения, то
при удалении выскочит ошибка. Скажет, что если действительно хочешь всё
потерять то используй ключ **-D**.

.. code-block:: bash

        git branch -d perov

Удаление ненужной ветки на сервере.

.. code-block:: bash

        git push origin --delete perov

Теперь как обновлять свой форк (в частности ветку **develop**):

Добавляем удалённый репозиторий Технокома (достаточно один раз)

.. code-block:: bash

        git remote add upstream git@gitlab.com:tehnokom/siriuso-fronto.git

        или

        git remote add upstream https://gitlab.com/tehnokom/siriuso-fronto.git

И обновляем свой **develop** из удалённого (это делаем регулярно перед
слиянием своей рабочей ветки и **develop**)

.. code-block:: bash

        git checkout develop

        git pull --rebase upstream develop

Лайфхак для начинающего контрибьютора
-------------------------------------------------

Начать следует с форка репозитория проекта, который планируется
дополнить. Это делается нажатием кнопки **"Fork"** на странице репозитория
прямо на GitLab.

Склонируйте себе локально исходный репозиторий. Важно клонировать не ваш
форк, а репозиторий исходного проекта. С таким подходом ветка **develop**
будет постоянно указывать на исходный проект.

.. code-block:: bash

        git clone https://gitlab.com/tehnokom/siriuso-fronto.git

Перейдем в каталог с клоном и посмотрим, какие удалённые репозиторий
настроены.

.. code-block:: bash

        cd siriuso-fronto

        git remote -v

                origin https://gitlab.com/tehnokom/siriuso-fronto (fetch)

                origin https://gitlab.com/tehnokom/siriuso-fronto (push)

За редким исключением, всем удалённым репозиториям в Git присваиваются
имена. **По умолчанию первый добавленный репозиторий называется origin**, но
в нашем случае это не подходит. Репозиторий исходного проекта
переименуем в **upstream**, из чего будет понятно что оттуда делается только
**pull**.

.. code-block:: bash

        git remote rename origin upstream

Для верности можно бесповоротно сломать возможность сделать **push**,
запретить выгружать любые коммиты обратно. **В команде ниже вместо
disabled можно использовать любой другой несуществующий URL.**

.. code-block:: bash

        git remote set-url --push upstream disabled

С такой настройкой сделать **push** будет просто невозможно.

.. code-block:: bash

        git remote -v

                upstream https://gitlab.com/tehnokom/siriuso-fronto (fetch)

                upstream disabled (push)

        git push

                fatal: 'disabled' does not appear to be a git repository

                fatal: Could not read from remote repository.

                Please make sure you have the correct access rights

                and the repository exists.

Аналогично можно сделать с веткой **develop** даже если та указывает на ваш
собственный репозиторий: вы не сможете по ошибке выгрузить какие-то
коммиты прямо в ветку **develop**, минуя открытие мерджреквеста из отдельной ветки.

Мы готовы к тому, чтобы подключить ваш форк, упомянув его SSH-ссылкой.

.. code-block:: bash

                git remote add origin git@gitlab.com:{ваше имя пользователя gitlab}/siriuso-fronto.git

Ваш форк в Git будет называться **origin**, с тем расчетом чтобы команды в
обычных подсказках, которые показывает Git, работали без необходимости
что-то менять перед копированием и вставкой. Как, например, при первом
пуше из новой ветки:

.. code-block:: bash

        git push

                fatal: The current branch test has no upstream branch.

                To push the current branch and set the remote as upstream, use

                git push --set-upstream origin test

Что ещё?
~~~~~~~~

Можно запретить коммиты в **develop** хуком **.git/hooks/pre-commit** с таким 
содержанием:

.. code-block:: bash

        #!/bin/sh

        set -e -x

        test $(git rev-parse --abbrev-ref HEAD) != "develop"

С таким хуком при попытке коммита в **develop** будет выводиться ошибка:

.. code-block:: bash

        git checkout develop

        git commit

                + git rev-parse --abbrev-ref HEAD

                + test develop != develop

Готово!
~~~~~~~

Все готово к началу работы. Дальше каждый раз одно и то же.

1. Загружаем последние изменения с исходного проекта в ветку **develop**.

.. code-block:: bash

   git checkout develop

   git pull

2. Делаем новую ветку с понятным названием на основе ветки **develop**.

.. code-block:: bash
   
   git checkout -b fix-for-issue-42

   Switched to a new branch 'fix-for-issue-42'

3. Вносим всевозможные изменения, запускаем тесты и так далее. После чего коммитим изменения.

.. code-block:: bash
   
   git commit

4. Выгружаем коммиты с работой из ветки обратно на GitLab.

.. code-block:: bash
   
   git push --set-upstream origin fix-for-issue-42

        Total 0 (delta 0), reused 0 (delta 0)

        To gitlab.com mailto:git@gitlab.com:tehnokom/siriuso-fronto.git
        
        * [new branch] fix-for-issue-42 -> fix-for-issue-42

        Branch 'fix-for-issue-42' set up to track remote branch 'fix-for-issue-42' from 'origin'.

5. Открываем мерджреквест, ждем одобрения ведущих разработчиков.

6. Радуемся сообщению о принятии изменений.

Полезные ссылки и источники
---------------------------

https://losst.ru/kak-polzovatsya-gitlab

https://www.alexeykopytko.com/2018/github-contributor-guide/

Еще хочу предложить краем глаза пробежать эту главу учебника.

`https://git-scm.com/book/ru/v2/Ветвление-в-Git-О-ветвлении-в-двух-словах <https://git-scm.com/book/ru/v2/%D0%92%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-Git-%D0%9E-%D0%B2%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B8-%D0%B2-%D0%B4%D0%B2%D1%83%D1%85-%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D1%85>`__

.. |image0| image:: _static/contrib/image1.png
   :width: 6.69306in
.. |image1| image:: _static/contrib/image2.png
   :width: 6.69306in
.. |image2| image:: _static/contrib/image3.png
   :width: 6.69306in
.. |image3| image:: _static/contrib/image4.png
   :width: 6.69306in
.. |image4| image:: _static/contrib/image5.png
   :width: 6.69306in
.. |image5| image:: _static/contrib/image6.png
   :width: 6.69306in
.. |image6| image:: _static/contrib/image7.png
   :width: 6.69306in
.. |image7| image:: _static/contrib/image8.png
   :width: 5.66667in
.. |image8| image:: _static/contrib/image9.png
   :width: 6.69306in
.. |image9| image:: _static/contrib/image10.png
   :width: 6.69306in
.. |image10| image:: _static/contrib/image11.png
   :width: 6.69306in
.. |image11| image:: _static/contrib/image12.png
   :width: 6.69306in
.. |image12| image:: _static/contrib/image13.png
   :width: 5.71875in
.. |image13| image:: _static/contrib/image14.png
   :width: 5.69792in
.. |image14| image:: _static/contrib/image15.png
   :width: 6.69306in
.. |image15| image:: _static/contrib/image16.png
   :width: 6.22917in
.. |image16| image:: _static/contrib/image17.png
   :width: 6.01042in
.. |image17| image:: _static/contrib/image18.png
   :width: 6.69306in
.. |image18| image:: _static/contrib/image19.png
   :width: 6.02083in
.. |image19| image:: _static/contrib/image20.png
   :width: 7.35347in
